
{-# LANGUAGE GADTSyntax #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Internal where

import Data.List
import Data.Maybe
import Data.Word
import Data.Int

import qualified Data.IntMap.Strict as IntMap
import qualified Data.Map.Strict as Map

import Control.Monad
-- import Control.Monad.Trans.Except
-- import Control.Monad.Trans.Writer
-- import Control.Monad.Trans.Reader
-- import Control.Monad.Trans.State.Lazy/Strict

-- import Debug.Trace

import qualified Data.ByteString.Lazy as BL
-- import qualified Data.ByteString as BS

-- import qualified Data.Text as TS
-- import qualified Data.Text.Encoding as ES
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as EL

-- Is it worth writing an instance for NBTPayload, so that TagLists' contents
-- can be unboxed?
import qualified Data.Vector.Unboxed as VU
import qualified Data.Vector as VB

-- These only work for lazy bytestrings apparently
import Data.Binary.Get
import Data.Binary.Put

import System.IO
import Codec.Compression.GZip
-- import Codec.Compression.Zlib

import Data.Text.Prettyprint.Doc
import Data.Text.Prettyprint.Doc.Render.String

type SByte = Int8
type SShort = Int16
type SInt = Int32
type SLong = Int64
type SFloat = Float
type SDouble = Double
type UByte = Word8
type UShort = Word16

-- So far I've been trying to push as much info into the header as possible. I
-- should be doing the opposite: the header doesn't exist, we just parse a
-- TagType and use it for dispatch, to get a TAG, i.e. a value of type NBT.

data TagType = TEnd | TByte | TShort | TInt | TLong | TFloat | TDouble | TByteArray
             | TString | TList | TCompound | TIntArray | TLongArray
             deriving (Show, Eq, Enum, Bounded, Ord)

-- NOTE: should rework the code a bit so that, for TString, T...Array and TList,
-- the metadata (e.g. length) that was previously part of the payload (due to
-- how the NBT spec is written) is actually added to the header, so that it
-- can be used when printing. I already did this with the TList element type.
-- Is there a more elegant way? Have a separate "payload metadata" field/type?

-- Define accessors manually instead of with record syntax, since
-- want to deal with Maybe and tuple.
data NBTHeader where
  NBTHeader :: TagType -> Maybe (UShort, TL.Text) -> Maybe TagType -> NBTHeader
  deriving (Show, Eq)

headerId :: NBTHeader -> TagType
headerId (NBTHeader tagId _ _) = tagId

headerNameLen :: NBTHeader -> Maybe UShort
headerNameLen (NBTHeader _ Nothing _) = Nothing
headerNameLen (NBTHeader _ (Just (len, _)) _) = Just len

headerName :: NBTHeader -> Maybe TL.Text
headerName (NBTHeader _ Nothing _) = Nothing
headerName (NBTHeader _ (Just (_, str)) _) = Just str

headerNameInfo :: NBTHeader -> Maybe (UShort, TL.Text)
headerNameInfo (NBTHeader _ Nothing _) = Nothing
headerNameInfo (NBTHeader _ x _) = x

headerListType :: NBTHeader -> Maybe TagType
headerListType (NBTHeader TList _ justEltType) = justEltType
headerListType (NBTHeader _ _ _) = Nothing

-- Q. A TagList doesn't actually store the type of its payload elements. Should
--    I add this? Would be useful for e.g. printing.
-- A. Yes, but it would have to be added to the HEADER. This would require a
--    minor redesign...
data NBTPayload where
  TagByte :: SByte -> NBTPayload
  TagShort :: SShort -> NBTPayload
  TagInt :: SInt -> NBTPayload
  TagLong :: SLong -> NBTPayload
  TagFloat :: SFloat -> NBTPayload
  TagDouble :: SDouble -> NBTPayload
  TagByteArray :: VU.Vector SByte -> NBTPayload
  TagString :: TL.Text -> NBTPayload
  -- Shouldn't this be VB.Vector [NBT], where all the NBTs have a Nothing header?
  -- Wasn't that the point?
  TagList :: VB.Vector NBTPayload -> NBTPayload
  TagCompound :: [NBT] -> NBTPayload
  TagIntArray :: VU.Vector SInt -> NBTPayload
  TagLongArray :: VU.Vector SLong -> NBTPayload
  deriving (Show, Eq)

-- "Unboxed" NBTs don't even have IDs, so need Maybe for header.
data NBT = NBT {
  header :: Maybe NBTHeader,
  payload :: Maybe NBTPayload }
  deriving (Show, Eq)

-- Define an annotation type sth like this:
--   data Mode = Collapse | Expand
-- Then allow user to choose what kinds of tags to collapse/expand on a
-- case-by-case basis? Could be useful when inspect particular subtrees.

-- Use combinators and predefined Pretty instances (e.g. for Text, Int etc)
-- to construct things of type Doc. How to specify annotation type tho?
-- Is it empty by default, and have to alter/add them later?
tagToDoc :: NBT -> Doc ann
tagToDoc tag =
  (maybe emptyDoc hdrToDoc (header tag)) <+>
  (maybe emptyDoc (payloadToDoc (header tag)) (payload tag))

hdrToDoc :: NBTHeader -> Doc ann
hdrToDoc hdr =
  pretty (show (headerId hdr)) <+>
  pretty (maybe "-" (showElided 30) (headerName hdr)) <+>
  maybe emptyDoc (pretty . show) (headerListType hdr)

-- A TagList of TagCompounds will separate entries by blank lines. The first
-- entry also has a blank line above, which is not ideal, and it's not always
-- immediately clear why these parts have extra indentation, but it does make
-- it readable, and it would probably take a lot of code changes to fix, so
-- keep it for now.

-- Need access to header for TagList case only. Oddly, TagLists can be nested
-- and the inner ones can contain different types. In that case, fall back to
-- the default behaviour.
payloadToDoc :: Maybe NBTHeader -> NBTPayload -> Doc ann
payloadToDoc _ (TagByte x) = pretty x
payloadToDoc _ (TagShort x) = pretty x
payloadToDoc _ (TagInt x) = pretty x
payloadToDoc _ (TagLong x) = pretty x
payloadToDoc _ (TagFloat x) = pretty x
payloadToDoc _ (TagDouble x) = pretty x
payloadToDoc _ (TagByteArray _) = pretty "<...bytes...>"
payloadToDoc _ (TagString x) = pretty (showElided 30 x)
payloadToDoc maybeHdr (TagList xs) =
  case maybeHdr of
    (Just hdr) -> handleSpecial $ fromJust (headerListType hdr)
    Nothing -> defaultBehaviour
  where
    -- Don't indent twice if it's a list of compound tags
    handleSpecial TCompound =
      VB.foldl' vcatPreserveNesting emptyDoc (fmap (payloadToDoc Nothing) xs)
    handleSpecial _ = defaultBehaviour
    defaultBehaviour =
      nest 2 (VB.foldl' vcatPreserveNesting emptyDoc
              (fmap (payloadToDoc Nothing) xs))
payloadToDoc _ (TagCompound tags) =
  nest 2 (foldl' vcatPreserveNesting emptyDoc $ fmap tagToDoc tags)
payloadToDoc _ (TagIntArray _) = pretty "<...ints...>"
payloadToDoc _ (TagLongArray _) = pretty "<...longs...>"

vcatPreserveNesting :: Doc ann -> Doc ann -> Doc ann
vcatPreserveNesting x y = x <> line <> y

docToSimple :: Doc ann -> SimpleDocStream ann
docToSimple = layoutPretty layoutOpts

layoutOpts :: LayoutOptions
layoutOpts = LayoutOptions (AvailablePerLine 70 0.8)

-- n must be greater than 4
showElided :: (Show a) => Int -> a -> String
showElided n = elide . show
  where
    elide xs
      | (length xs) >= n = take (n - 4) xs ++ "...\""
      | otherwise = xs

-- Adds trailing spaces so string length is n, minimum.
showPadded :: (Show a) => Int -> a -> String
showPadded n = pad . show
  where
    pad xs
      | (length xs) < n = xs ++ (replicate (n - length xs) ' ')
      | otherwise = xs

-- This is the user-facing function.
showTag :: NBT -> String
showTag = renderString . docToSimple . tagToDoc

-- Most of my functions should look like these two
-- getNBT :: Get NBT
-- putNBT :: NBT -> Put

-- Then a couple will look like these (could replace IO with sth that can
-- better handle exceptions)
-- readNBT :: IO BL.ByteString -> IO NBT
-- writeNBT :: IO BL.ByteString -> IO NBT -> IO ()

-- Then in "main" or "run", get the handles etc. For region files, will
-- need to handle (de)compression within the Get/Put monads. Should be
-- ok, as they only operate on ByteStrings.

-- ADD ERROR HANDLING to these three
readNBTFile :: FilePath -> Bool -> Get a -> IO a
readNBTFile filePath unZip getFn = do
  file <- openFile filePath ReadMode
  -- BL.hGetContents fully reads the file, then closes it, but I ensure it's
  -- closed afterwards for safety.
  bs <- BL.hGetContents file
  return $ runGet getFn (if unZip then (decompress bs) else bs)

{-
-- Could change this to take an "a", not "IO a", by fiddling at call site?
writeNBTFile :: FilePath -> Bool -> (a -> Put) -> IO a -> IO ()
writeNBTFile filePath zipUp putFn outData = do
  file <- openFile filePath WriteMode
  contents <- outData
  let bs = runPut (putFn contents)
  BL.hPut file (if zipUp then (compress bs) else bs)
  hClose file
-}

runTest :: FilePath -> Bool -> IO ()
runTest filePath gzip = do
  value <- readNBTFile filePath gzip getNBTTag
  print value
  -- writeNBTFile "data/test.dat" gzip putNBTHeader (return value)
  -- value' <- readNBTFile "data/test.dat" gzip getNBTHeader
  -- print (value == value')

-- Use Get's MonadFail/Plus instance for graceful error handling?
-- Note that guard function is not defined in Get's instances,
-- but in Control.Monad
getNBTHeader :: Get NBTHeader
getNBTHeader = do
  idn <- getWord8
  -- _ <- trace ("getNBTHeader: idn: " ++ show idn) (return ())
  -- Throws error if Nothing
  let tagType = fromJust (idToTagType (fromIntegral idn))
  -- _ <- trace ("getNBTHeader: tagType: " ++ show tagType) (return ())
  if tagType == TEnd
    then return $ NBTHeader tagType Nothing Nothing
    else do
    len <- getWord16be
    -- _ <- trace ("getNBTHeader: len: " ++ show len) (return len)
    str <- getLazyByteString (fromIntegral len)
    -- _ <- trace ("getNBTHeader: str: " ++ show (EL.decodeUtf8 str)) (return ())
    if tagType /= TList
      then return $ NBTHeader tagType (Just (len, (EL.decodeUtf8 str))) Nothing
      else do
      -- Need to PEEK at the next byte without consuming it, otherwise
      -- getTagListPayload would need an argument, and therefore have a different
      -- type signature to the other payload-getting functions, breaking the
      -- mapping code.
      eltId <- lookAhead getWord8
      let eltType = fromJust (idToTagType (fromIntegral eltId))
      return $ NBTHeader tagType (Just (len, (EL.decodeUtf8 str))) (Just eltType)

{-
putNBTHeader :: NBTHeader -> Put
putNBTHeader hdr = do
  -- Throws error if Nothing
  putWord8 (fromJust (tagTypeToId (headerId hdr)))
  case (headerNameInfo hdr) of
    Just (len, str) -> do
      putWord16be len
      putLazyByteString (EL.encodeUtf8 str)
      return ()
    Nothing -> return ()
-}

getSByte :: Get SByte
getSByte = fromIntegral <$> getWord8

getSShort :: Get SShort
getSShort = fromIntegral <$> getWord16be

getSInt :: Get SInt
getSInt = fromIntegral <$> getWord32be

getSLong :: Get SLong
getSLong = fromIntegral <$> getWord64be

-- Are the floats and doubles big-endian too? Check.
getSFloat :: Get SFloat
getSFloat = realToFrac <$> getFloatbe

getSDouble :: Get SDouble
getSDouble = realToFrac <$> getDoublebe

getTagBytePayload :: Get NBTPayload
getTagBytePayload = TagByte <$> getSByte

getTagShortPayload :: Get NBTPayload
getTagShortPayload = TagShort <$> getSShort

getTagIntPayload :: Get NBTPayload
getTagIntPayload = TagInt <$> getSInt

getTagLongPayload :: Get NBTPayload
getTagLongPayload = TagLong <$> getSLong

getTagFloatPayload :: Get NBTPayload
getTagFloatPayload = TagFloat <$> getSFloat

getTagDoublePayload :: Get NBTPayload
getTagDoublePayload = TagDouble <$> getSDouble

-- Try rewriting in applicative style to see if the side-effects are performed
-- in the right order. Should be, but lazy eval...
getTagStringPayload :: Get NBTPayload
getTagStringPayload = do
  -- Call getWord16be directly to avoid double fromIntegral? Or rename/define
  -- so I can call getSShort directly?
  len <- getWord16be -- SShort
  str <- getLazyByteString (fromIntegral len) -- needs fromIntegral: expects Int64
  return (TagString (EL.decodeUtf8 str))

-- Need to accumulate a VU.Vector SByte.
-- Again, we do a double fromIntegral here. Fix?
getTagByteArrayPayload :: Get NBTPayload
getTagByteArrayPayload = do
  len <- getSInt
  TagByteArray <$> (VU.replicateM (fromIntegral len) getSByte)

getTagIntArrayPayload :: Get NBTPayload
getTagIntArrayPayload = do
  len <- getSInt
  TagIntArray <$> (VU.replicateM (fromIntegral len) getSInt)

getTagLongArrayPayload :: Get NBTPayload
getTagLongArrayPayload = do
  len <- getSInt
  TagLongArray <$> (VU.replicateM (fromIntegral len) getSLong)

-- "In some instances, empty lists may be represented as a list of Byte tags
-- rather than a list of the correct type, or as a list of End tags in newer
-- versions of Minecraft." (I hope they just mean Byte tag payloads, and not
-- entire Byte tags, because this will break my parser as well as my types.)

-- If the latter is the case (when reading in), just consume "len" bytes
-- and store them to be written out later. Could sanitise the output myself
-- (though Minecraft might not like that, and it would violate the
-- "put . get == id" idea). Must do this within the Get monad.

-- If the former is ever the case in modern Minecraft files, then I'll need
-- to redesign, because then TagLists would have to be able to contain NBTs
-- too, not just NBTPayloads. For now, just throw an error in this case?

getTagListPayload :: Get NBTPayload
getTagListPayload = do
  idn <- getWord8
  -- Throws error if Nothing
  let tagType = fromJust (idToTagType (fromIntegral idn))
  len <- getSInt
  if tagType == TEnd
    then TagList <$> (VB.replicateM (fromIntegral len) getTagBytePayload)
    else TagList <$> (VB.replicateM (fromIntegral len) (getPayloadOf tagType))

-- List of tags, followed by TAG_End (which is omitted from repr, but will
-- be added back on write out.
getTagCompoundPayload :: Get NBTPayload
getTagCompoundPayload = TagCompound <$> fmap reverse (recurse (return []))
  where
    recurse :: Get [NBT] -> Get [NBT]
    recurse tags = do
      tag <- getNBTTag
      -- _ <- trace ("getTagCompoundPayload: tag: " ++ show tag) (return ())
      if (TEnd == headerId (fromJust (header tag)))
        then tags
        else recurse ((liftM2 (:)) (return tag) tags)

idsToTagTypes :: IntMap.IntMap TagType
idsToTagTypes = IntMap.fromList (zip [0..] [minBound .. maxBound])

idToTagType :: Int -> Maybe TagType
idToTagType = (flip IntMap.lookup) idsToTagTypes

tagTypesToIds :: Map.Map TagType UByte
tagTypesToIds = Map.fromList (zip [minBound .. maxBound] [0 :: UByte ..])

tagTypeToId :: TagType -> Maybe UByte
tagTypeToId = (flip Map.lookup) tagTypesToIds

-- Throws error is called with any number not in range [1..12] incl.
getPayloadOf :: TagType -> Get NBTPayload
getPayloadOf tagType = fromJust (Map.lookup tagType tagTypesToGetPayloads)

-- TagEnd doesn't have one
tagTypesToGetPayloads :: Map.Map TagType (Get NBTPayload)
tagTypesToGetPayloads = Map.fromList (zip [TByte .. maxBound]
                                     [getTagBytePayload,
                                     getTagShortPayload,
                                     getTagIntPayload,
                                     getTagLongPayload,
                                     getTagFloatPayload,
                                     getTagDoublePayload,
                                     getTagByteArrayPayload,
                                     getTagStringPayload,
                                     getTagListPayload,
                                     getTagCompoundPayload,
                                     getTagIntArrayPayload,
                                     getTagLongArrayPayload])

getNBTTag :: Get NBT
getNBTTag = do
  hdr <- getNBTHeader
  -- _ <- trace ("getNBTTag: hdr: " ++ show hdr) (return ())
  let tagType = (headerId hdr)
  -- _ <- trace ("getNBTTag: tagType: " ++ show tagType) (return ())
  case tagType of
    TEnd -> return $ NBT (Just hdr) Nothing
    _ -> NBT <$> return (Just hdr) <*> fmap Just (getPayloadOf tagType)
