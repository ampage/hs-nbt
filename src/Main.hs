module Main where

import Lib

main :: IO ()
main = do
  -- runTest "data/level.dat" True
  -- runTest "data/map_3.dat" True
  -- runTest "data/Player-nan-value.dat" True
  -- runTest "data/idcounts.dat" True
  -- runTest "data/player.dat" True
  -- runTest "data/bigtest.dat" True
  runTest "data/hello-world-uncomp.dat" False
  return ()
